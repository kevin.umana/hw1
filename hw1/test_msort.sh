#test 1: random sequence of integers 
CILK_WORKERS=16 ./msort 100 test_msort_1.txt > result.txt
./compare 100 result.txt result_msort_1.txt
#test 2: inverted sequence
CILK_WORKERS=16 ./msort 1024 test_msort_2.txt > result.txt
./compare 1024 result.txt result_msort_2.txt
#test 3: sequence of same number
CILK_WORKERS=16 ./msort 289 test_msort_3.txt > result.txt
./compare 289 result.txt result_msort_3.txt
#test 4: random long sequence
CILK_WORKERS=16 ./msort 10000 test_msort_4.txt > result.txt
./compare 10000 result.txt result_msort_4.txt
# removing result.txt file
rm -f result.txt
