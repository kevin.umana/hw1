/**
 * Costa Rica Institute of Technology
 * School of Computing
 * ce5504: High Performance Computing
 * Instructor Esteban Meneses, PhD (emeneses@ic-itcr.ac.cr)
 * Student: 
 * Cilk Plus parallel Conway's Game of Life.
 */

#include <cstdio>
#include <cstdlib>
#include <cilk/cilk.h>
#include "timer.h"
#include "io.h"

// Main method      
int main(int argc, char* argv[]) {
	int N,M;
	int **World;
	double elapsedTime;

	// checking parameters
	if (argc != 3 && argc != 4) {
		cout << "Parameters: <N> <M> [<file>]" << endl;
		return 1;
	}
	N = atoi(argv[1]);
	M = atoi(argv[2]);

	// allocating matrices
	World = new int*[N];
	for (int i=0; i<N; i++){
		World[i] = new int[N];
	}

	// reading files (optional)
	if(argc == 4){
		readMatrixFile(World,N,argv[3]);
	}

	// starting timer
	timerStart();

	// YOUR CODE GOES HERE

	// testing the results is correct
	if(argc == 4){
		printMatrix(World,N);
	}
	
	// stopping timer
	elapsedTime = timerStop();

	cout << "Duration: " << elapsedTime << " seconds" << std::endl;

	// releasing memory
	for (int i=0; i<N; i++) {
		delete [] World[i];
	}
	delete [] World;

	return 0;	
}

