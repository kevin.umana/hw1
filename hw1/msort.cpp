/**
 * Costa Rica Institute of Technology
 * School of Computing
 * MC8836: Parallel Computing
 * Instructor Esteban Meneses, PhD (emeneses@ic-itcr.ac.cr)
 * Cilk Plus parallel merge sort algorithm.
 * The parallel programming pattern used is Fork-join.
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cilk/cilk.h>
#include "timer.h"
#include "io.h"

#define MAX_VALUE 10000

// Function to allocate array. 
int *alloc_array(int N){
	int *ptr;
	
	ptr = new int[N];
	return ptr;
}

// Function to free array
void free_array(int* ptr){
	delete [] ptr;
}

// Recursive merge sort function
void msort_rec(int *A, int N, int *tmp){
	int half;
	int *B, *C, j, k;

	// checking for trivial cases
	if(N < 2) return;

	// creating sub-arrays
	half = N/2;
	B = A;
	C = A + half;
	
	// divide (sorting each half)
	cilk_spawn msort_rec(B,half,tmp);
	msort_rec(C,N-half,tmp+half);
	cilk_sync;

	// conquer (merging the two sub-arrays)
	j=k=0;
	for(int i=0; i<N; i++){
		if(j >= half) {
			memcpy(tmp+i,C+k,sizeof(int)*(N-half-k));
			break;
		}
		if(k >= (N-half)) {
			memcpy(tmp+i,B+j,sizeof(int)*(half-j));
			break;
		}
		if(B[j] < C[k]) tmp[i] = B[j++];
		else tmp[i] = C[k++];
	}
	memcpy(A,tmp,sizeof(int)*N);

}

// Function implementing merge sort algorithm
void msort(int *A, int N){
	int half, *tmp;

	// allocating temporary buffer
	tmp = alloc_array(N);
	
	// calling recursive function
	msort_rec(A,N,tmp);

	// deallocating memory
	free_array(tmp);
}
	
// Main method      
int main(int argc, char* argv[]) {
	int N;
	int *A;
	double elapsedTime;

	// checking parameters
	if (argc != 2 && argc != 3) {
		cout << "Parameters: <N> [<file>]" << endl;
		return 1;
	}
	N = atoi(argv[1]);

	// allocating array
	A = new int[N];

	// reading files (optional)
	if(argc == 3){
		readArrayFile(A,N,argv[2]);
	} else {
		srand48(time(NULL));
		for(int i=0; i<N; i++){
			A[i] = lrand48() % MAX_VALUE;
		}
	}

	// starting timer
	timerStart();

	// calling merge sort algorithm function
	msort(A,N);

	// testing the results is correct
	if(argc == 3){
		printArray(A,N);
	}
	
	// stopping timer
	elapsedTime = timerStop();

	cout << "Duration: " << elapsedTime << " seconds" << std::endl;

	// releasing memory
	delete [] A;

	return 0;	
}

