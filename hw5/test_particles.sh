# test 1: some particles per core
/usr/lib64/openmpi/bin/mpiexec -n 31 ./particles 496 test_particles_1.txt > tmp.txt
tail -n +3 tmp.txt > result.txt
rm -f tmp.txt
./compare 2981 result.txt result_particles_1.txt 0.001
# test 2: exactly one particle per core
/usr/lib64/openmpi/bin/mpiexec -n 31 ./particles 31 test_particles_2.txt > tmp.txt
tail -n +3 tmp.txt > result.txt
rm -f tmp.txt
./compare 191 result.txt result_particles_2.txt 0.001
# test 3: one particle replicated
/usr/lib64/openmpi/bin/mpiexec -n 31 ./particles 31 test_particles_3.txt > tmp.txt
tail -n +3 tmp.txt > result.txt
rm -f tmp.txt
./compare 191 result.txt result_particles_3.txt 0.001
# test 4: several particles per core
#export MV2_IBA_EAGER_THRESHOLD=256000000
/usr/lib64/openmpi/bin/mpiexec -n 31 ./particles 31000 test_particles_4.txt > tmp.txt
tail -n +3 tmp.txt > result.txt
rm -f tmp.txt
echo "OK"
