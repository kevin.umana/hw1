/**
 * Costa Rica Institute of Technology
 * School of Computing
 * ce5504: High Performance Computing
 * Instructor Esteban Meneses, PhD (emeneses@ic-itcr.ac.cr)
 * Student: 
 * Cilk Plus parallel merge sort algorithm.
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cilk/cilk.h>
#include "timer.h"
#include "io.h"

#define MAX_VALUE 10000

// Function implementing merge sort algorithm
void msort(int *A, int N){

	// YOUR CODE GOES HERE

}
	
// Main method      
int main(int argc, char* argv[]) {
	int N;
	int *A;
	double elapsedTime;

	// checking parameters
	if (argc != 2 && argc != 3) {
		cout << "Parameters: <N> [<file>]" << endl;
		return 1;
	}
	N = atoi(argv[1]);

	// allocating array
	A = new int[N];

	// reading files (optional)
	if(argc == 3){
		readArrayFile(A,N,argv[2]);
	} else {
		srand48(time(NULL));
		for(int i=0; i<N; i++){
			A[i] = lrand48() % MAX_VALUE;
		}
	}

	// starting timer
	timerStart();

	// calling merge sort algorithm function
	msort(A,N);

	// testing the results is correct
	if(argc == 3){
		printArray(A,N);
	}
	
	// stopping timer
	elapsedTime = timerStop();

	cout << "Duration: " << elapsedTime << " seconds" << std::endl;

	// releasing memory
	delete [] A;

	return 0;	
}

